const jwt = require('jsonwebtoken')
require('dotenv').config()
//const config = require('../config.json').security

const config = {
    secPass     : process.env.SEG_PASS,
    timeToken   : process.env.SEG_TIME
}

CreaToken = async (data) => {
    return await jwt.sign(data, config.secPass, { expiresIn: config.timeToken })
}

VerificarToken = async (req, res, next) => {
    try {
        const bearerHeader = req.headers['authorization']
        if (typeof bearerHeader !== 'undefined') {
            const bearerToken = bearerHeader.split(' ')[1]
            let token = await jwt.verify(bearerToken, config.secPass)
            req.token = token
            next()
        } else {
            res.sendStatus(403)
        }
    } catch (error) {
        res.sendStatus(403)
    }

}

module.exports = {
    CreaToken,
    VerificarToken
}
