const express = require('express')
const seguridad = require('../security/Security')
const  usuarioController  = require("../controllers/usuarioController")

const route = express.Router()

route.get('/usuario',seguridad.VerificarToken,usuarioController.ListaCredenciales)
route.post('/iniciosesion',usuarioController.ValidarCredenciales)
route.post('/usuario/actualizar',seguridad.VerificarToken,usuarioController.ActualizarUsuario)
route.get('/usuario/switch',seguridad.VerificarToken,usuarioController.SwitchUsuarioActivo)
module.exports = route