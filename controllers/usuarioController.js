const seguridad = require('../security/Security')
const mssql = require('mssql')
let UsuarioHH = require("../models/UsuarioHH")
require('dotenv').config()

const config =
{
    user    : process.env.SQL_USER,
    password: process.env.SQL_PASS,
    server  : process.env.SQL_SERVER,
    database: process.env.SQL_DB
}


ValidarCredenciales = async (req, res) => {
    try {
        const { user, pass } = req.body
        console.log(`${user} , ${pass}`);
        let pool = await mssql.connect(config)
        let result = await pool.request()
            .input('user', mssql.VarChar(50), user)
            .input('pass', mssql.VarChar(50), pass)
            .execute('spVal_usuario_inicioSesionWeb')
        const neto = result.recordset[0]
        if (neto.CODE === 0) {
            const token = await seguridad.CreaToken(neto)
            const data = {
                MESSAGE: 'usuario válido',
                CODE: 0,
                token
            }
            res.status(200).json(data)
        }
        else {
            res.status(200).json({ CODE: -1, MESSAGE: 'usuario inválido' })
        }
    } catch (error) {
        res.status(500).json({ error })
    }
}

ListaCredenciales = async (req, res) => {
    let pool = await mssql.connect(config)
    let result = await pool.request()
        .execute('spRec_usuario_listaSimple')

    const neto = result.recordset
    res.status(200).json(neto)
}

ActualizarUsuario = async (req, res) => {
    const { id, nombre, usuario, contrasenna, activo, admin } = req.body.usuario
    contrasenna === '' ? null : contrasenna
    console.log(activo === true ? 1 : 0)
    let pool = await mssql.connect(config)
    let result = await pool.request()
        .input('idUsuario', mssql.Int, id)
        .input('nombre', mssql.VarChar(50), nombre)
        .input('usuario', mssql.VarChar(50), usuario)
        .input('clave', mssql.VarChar(50), contrasenna)
        .input('activo', mssql.Bit, activo === true ? 1 : 0)
        .input('admin', mssql.Bit, admin === true ? 1 : 0)
        .execute('spUpd_Usuario')
    const neto = result.recordset[0]
    const response = {
        CODE: neto.RESULT,
        MESSAGE: neto.Mensaje
    }
    res.status(200).json(response)
}

SwitchUsuarioActivo = async (req, res) => {
    try {
        const { idUsuario } = req.query
        let pool = await mssql.connect(config)
        let result = await pool.request()
            .input('idUsuario', mssql.Int, idUsuario)
            .execute('spUpd_Usuario_switchActivo')

        const neto = result.recordset[0]
        res.status(201).json(neto)
    } catch (err) {
        res.status(500).json({ CODE: -3, MESSAGE: err })
    }
}

module.exports = { ValidarCredenciales, ListaCredenciales, ActualizarUsuario, SwitchUsuarioActivo }