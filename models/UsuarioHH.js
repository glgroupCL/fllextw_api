module.exports = class UsuarioHH {
    constructor(id,nombre,correo,usuario,contrasenna=null,admin=0,activo=true){
        this.id         =   id
        this.nombre     =   nombre
        this.correo     =   correo
        this.usuario    =   usuario
        this.contrasenna=   contrasenna
        this.admin      =   admin
        this.activo     =   activo
    }
}