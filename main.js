const express = require("express")
const morgan = require("morgan")
const config = require("./config.json").connection
const app = express()
const usuarioRoute = require("./routes/usuarioRoute")
var cors = require('cors')
require('dotenv').config()

app.use(cors())
app.use(express.urlencoded({extended:"false"}))
app.use(express.json())
app.use(morgan("dev"))
app.use("/",usuarioRoute)

app.set('port',process.env.PORT || config.port || "3007")
app.get('/',(req,res)=>{
    res.status(200).json({response:"hey i'm mr meeseeks look at me"})
})
app.listen(app.get('port'),()=>{
    console.log(`api falabella ready to work on port ${app.get('port')}!!`)
})